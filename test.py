import asyncio
import time
import aiohttp


async def download_site(session, url):
    async with session.get(url) as response:
        print("Read {0} from {1}".format(response.content_length, url))


async def main():
    sites = [
        "https://itvdn.com/",
        "https://dou.ua/",
    ] * 80
    start_time = time.time()
    async with aiohttp.ClientSession() as session:
        for url in sites:
            await download_site(session, url)

    duration = time.time() - start_time
    print(f"Downloaded {len(sites)} sites in {duration} seconds")


asyncio.run(main())
